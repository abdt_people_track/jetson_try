import cv2


def read_write_video(input_path, output_path):
    cap = cv2.VideoCapture(input_path)
    fwidth, fheight = int(cap.get(3)), int(cap.get(4))
    fourcc = cv2.VideoWriter_fourcc(*"XVID")
    out = cv2.VideoWriter(output_path, fourcc, 25.0, (fwidth, fheight))
    while cap.isOpened():
        check, frame = cap.read()
        if check:
            out.write(frame)
            cv2.imshow("kek", frame)
        else:
            break
        if cv2.waitKey(1) & 0xFF == ord("q"):
            break
        cap.release()
        out.release()


read_write_video("rtsp://admin:admin@192.168.0.103:554", "./test.avi")
