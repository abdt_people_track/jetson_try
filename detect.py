import cv2
import numpy as np
import sort
import darknet
import pandas as pd
from time import sleep
from datetime import datetime
import logging
from pytz import timezone
import argparse
from flask import Response, Flask
import threading

app = Flask(__name__)
global video_frame
video_frame = None


class BusCamera(object):
    def __init__(self, name_camera, cfg, obj_data, weights, path_to_input_video, path_to_output_video, logger, up=340, down=280, out_width=800, out_height=800, batch_size=1, max_age=5, thresh=0.5, fourcc='MJPG'):
        self.name_camera = name_camera
        self.path_to_input_video = path_to_input_video
        self.path_to_output_video = path_to_output_video
        self.network, self.class_names, _ = darknet.load_network(cfg, obj_data, weights, batch_size)
        self.tracker = sort.Sort(max_age)
        self.fourcc = cv2.VideoWriter_fourcc(*fourcc)
        self.width = darknet.network_width(self.network)
        self.height = darknet.network_height(self.network)
        self.out_width = out_width
        self.out_height = out_height
        self.thresh = thresh  # confidence threshold
        self.width_scale = self.out_width / self.width
        self.height_scale = self.out_height / self.height
        self.down = int(down * self.height_scale)
        self.up = int(up * self.height_scale)
        self.line_centr = int((self.up - self.down) / 2) + self.down
        self.outcome = 0
        self.income = 0
        self.dic = {}
        self.logs = []
        self.logger = logger

    def __str__(self):
        return f"Всего зашло: {self.income}, Всего вышло: {self.outcome}"

    def __call__(self):
        self.video_writer()

    def video_writer(self):
        out = cv2.VideoWriter(self.path_to_output_video, self.fourcc, 24.0, (self.out_width, self.out_height))
        while True:
            cap = cv2.VideoCapture(self.path_to_input_video)
            while cap.isOpened():
                ret, frame = cap.read()
                if ret:
                    detects, doors, frame = self.prepare_to_tracking(frame)
                    frame = self.draw_door(frame, doors)
                    trackers = self.tracker.update(detects)
                    self.track_and_draw_and_write(trackers, frame, out=out)
                else:
                    print(
                        "Some error... ret is False ... Camera out of reach ... go avoid ..."
                    )
                    self.logger.warning("unknown")
                    # self.log_write('unknown')
                    break
                if cv2.waitKey(1) & 0xFF == ord("q"):
                    break
            cap.release()
            cv2.destroyAllWindows()
            sleep(10)
        out.release()

    def image_detection(self, image):
        darknet_image = darknet.make_image(self.width, self.height, 3)
        image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image_rgb = cv2.cvtColor(image_gray, cv2.COLOR_GRAY2RGB)
        image_resized = cv2.resize(image_rgb, (self.width, self.height), interpolation=cv2.INTER_LINEAR)
        darknet.copy_image_from_bytes(darknet_image, image_resized.tobytes())
        detections = darknet.detect_image(self.network, self.class_names, darknet_image, self.thresh)
        darknet.free_image(darknet_image)
        return detections

    def prepare_to_tracking(self, frame):
        detections = self.image_detection(frame)
        doors = [darknet.bbox2points(self.resize_bbox(i[2])) for i in detections if i[0] == "door"]
        persons = [i for i in detections if i[0] == "Person"]
        if len(persons) != 0:
            detects = np.zeros((len(persons), 5))
            for i, detection in enumerate(persons):
                confidence = float(detection[1]) / 100  # [0;1]
                bbox = self.resize_bbox(detection[2])
                x1, y1, x2, y2 = darknet.bbox2points(bbox)
                detects[i, :] = np.array([x1, y1, x2, y2, confidence])
        else:
            detects = np.empty((0, 5))
        frame = cv2.resize(frame, (self.out_width, self.out_height), interpolation=cv2.INTER_LINEAR)
        return detects, doors, frame

    def calc_center_bbox(self, point1, point2):
        x1, y1 = point1
        x2, y2 = point2
        return (int(x1 + (x2 - x1) / 2), int(y1 + (y2 - y1) / 2))

    def draw_door(self, frame, doors):
        for d in doors:
            point1 = (int(d[0]), int(d[1]))
            point2 = (int(d[2]), int(d[3]))
            frame = cv2.rectangle(frame, point1, point2, (0, 255, 0), thickness=2)
        return frame

    def resize_bbox(self, bbox):
        x, y, w, h = bbox
        return (
            x * self.width_scale,
            y * self.height_scale,
            w * self.width_scale,
            h * self.height_scale,
        )

    def track_and_draw_and_write(self, trackers, frame, out):
        global video_frame
        for d in trackers:
            point1 = (int(d[0]), int(d[1]))
            point2 = (int(d[2]), int(d[3]))
            id_track = int(d[4])
            center_point = self.calc_center_bbox(point1, point2)
            if id_track not in self.dic:
                self.dic[id_track] = None
            self.counter(id_track, center_point[1])
            result_img = cv2.rectangle(frame, point1, point2, (255, 0, 0), thickness=2)
            result_img = cv2.circle(result_img, center_point, 2, (255, 0, 0), thickness=5)
            result_img = cv2.line(result_img, (0, self.up), (self.out_width, self.up), (255, 0, 0))
            result_img = cv2.line(result_img, (0, self.down), (self.out_width, self.down), (255, 0, 0))
            result_img = cv2.putText(result_img, "{}".format(id_track), (int(d[0]), int(d[1]) - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
            result_img = cv2.rectangle(result_img, (self.out_width - 120, 5), (self.out_width, 65), (255, 255, 255), -1)
            result_img = cv2.putText(result_img, f'income: {self.income}', (self.out_width - 100, 25),  cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
            result_img = cv2.putText(result_img, f'Outcome: {self.outcome}', (self.out_width - 100, 50),  cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
        if len(trackers) != 0:
            # out.write(result_img)
            # cv2.imshow("bus", result_img)
            video_frame = result_img.copy()
        else:
            frame = cv2.rectangle(frame, (self.out_width - 120, 5), (self.out_width, 65), (255, 255, 255), -1)
            frame = cv2.putText(frame, f'income: {self.income}', (self.out_width - 100, 25),  cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,0,0), 2)
            frame = cv2.putText(frame, f'Outcome: {self.outcome}', (self.out_width - 100, 50),  cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,0,0), 2)
            # out.write(frame)
            # cv2.imshow("bus", frame)
            video_frame = frame.copy()
        return None

    def counter(self, id_track, y):
        if self.dic[id_track] == "Untracked":
            return None
        if y >= self.down and y <= self.up:
            if self.dic[id_track] is None:
                if y < self.line_centr:
                    self.dic[id_track] = "Down"
                else:
                    self.dic[id_track] = "Up"
        else:
            if self.dic[id_track] == "Up" and y < self.down:
                self.income += 1
                #  логируем вход. указываем время, имя камеры, income
                self.logger.info("income")
                # self.log_write('income')
                self.dic[id_track] = "Untracked"
            if self.dic[id_track] == "Down" and y > self.up:
                self.outcome += 1
                #  логируем выход. указываем время, имя камеры, outcome
                self.logger.info("outcome")
                # self.log_write('outcome')
                self.dic[id_track] = "Untracked"
        return None

    def log_write(self, status):
        time = pd.to_datetime("today").replace(microsecond=0)
        self.logs.append([time, self.name_camera, status])

    def upload_logs_csv(self):
        data = pd.DataFrame(self.logs, columns=["time", "camera_name", "status"])
        data.to_csv(f"logs_from_{str(datetime.now()).split()[0]}.csv", index=False)

    def concat_two_frames(self, data1, data2):
        """data1 - first .csv file
        data2 - second .csv file
        """
        data1 = pd.read_csv(data1)
        data2 = pd.read_csv(data2)
        data = data1.append(data2, ignore_index=True)
        data.time = pd.to_datetime(data.time)
        data.sort_values(by="time", inplace=True)
        data.to_csv(f"logs_from_{str(datetime.now()).split()[0]}.csv", index=False)


@app.route("/")
def streamFrames():
    return Response(encodeFrame(), mimetype="multipart/x-mixed-replace; boundary=frame")


def encodeFrame():
    while True:
        global video_frame
        if video_frame is None:
            continue
        return_key, encoded_image = cv2.imencode(".jpg", video_frame)
        if not return_key:
            continue

        # Output image as a byte array
        yield (
            b"--frame\r\n"
            b"Content-Type: image/jpeg\r\n\r\n" + bytearray(encoded_image) + b"\r\n"
        )


def init_logger(camera_name, output_file_name="result_video/camera_log.txt"):
    logger = logging.getLogger(camera_name)
    logger.setLevel(logging.INFO)
    file_handler = logging.FileHandler(output_file_name)
    logger.addHandler(file_handler)
    logging.Formatter.converter = lambda *args: datetime.now(
        tz=timezone("Europe/Moscow")
    ).timetuple()
    formatter = logging.Formatter(
        fmt="%(asctime)s,%(name)s,%(message)s", datefmt="%Y-%m-%d %H:%M:%S"
    )
    file_handler.setFormatter(formatter)
    logger.propagate = False
    return logger


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--camera", required=True, type=int)
    args = vars(parser.parse_args())
    # print(args["camera"])
    # print(type(args["camera"]))

    rtsp = {
        1: "rtsp://178.207.8.252:6604/MTUsMywyMzcwMjgsMCwwLDAsMA==",
        2: "rtsp://178.207.8.252:6604/MTUsMywyMzcwMjgsMSwwLDAsMA==",
        3: "rtsp://178.207.8.252:6604/MTUsMywyMzcwMjgsMywwLDAsMA==",
    }

    lines = {1: (330, 270), 2: (340, 280), 3: (340, 280)}

    camera_name = f'camera{args["camera"]}'
    logger = init_logger(camera_name)
    up, down = lines[args["camera"]]
    camera = BusCamera(
        name_camera=camera_name,
        cfg="cfg/yolo-obj.cfg",
        obj_data="data/obj.data",
        weights="backup/yolo-obj_best.weights",
        path_to_input_video=rtsp[args["camera"]],
        path_to_output_video="result_video/66.avi",
        logger=logger,
        up=up,
        down=down,
    )

    # Create a thread and attach the method that captures the image frames, to it
    process_thread = threading.Thread(target=camera)
    process_thread.daemon = True

    # Start the thread
    process_thread.start()

    # start the Flask Web Application
    # While it can be run on any feasible IP, IP = 0.0.0.0 renders the web app on
    # the host machine's localhost and is discoverable by other machines on the same network
    app.run("0.0.0.0", port=f"800{args["camera"]}")


# Вводные данные от личного кабинета