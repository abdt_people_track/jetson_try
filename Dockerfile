FROM nvcr.io/nvidia/pytorch:20.03-py3

# Install dependencies (pip or conda)
RUN pip install -U gsutil

WORKDIR /bus/darknet
RUN pip install filterpy
# Copy contents
COPY . /bus/darknet

RUN make
# ENV Camera_id=2
# # $Camera_id значение камеры
# #CMD echo "$Camera_id"
# CMD python detect_without_flask.py -c $Camera_id


# ---------------------------------------------------  Extras Below  ---------------------------------------------------

# build
# docker build -t ilnazsr/darknet .

# Если нам нужно, чтобы было отображение на экране, то нужно дать права:
# xhost local:root
# Запускаем образ и монтируем доп папки:
# docker run -it --gpus all --ipc=host --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" -v /home/ilnar/abct/video_yolo/result_video:/bus/darknet/result_video -e DISPLAY=$DISPLAY shafigullinik/darknet bash

# docker run -it --gpus all --ipc=host --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" -v /home/ilnazsr/ABDT/result_video:/bus/darknet/result_video -v /home/ilnazsr/ABDT/source_video:/bus/darknet/source_video -v /home/ilnazsr/ABDT/GitLabABDT/jetson_try/detect_without_flask.py:/bus/darknet/detect_without_flask.py -e DISPLAY=$DISPLAY -e "Camera_id=4" ilnazsr/darknet
# docker run -it --gpus all --ipc=host --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" -v /home/ilnazsr/ABDT/result_video:/bus/darknet/result_video -v /home/ilnazsr/ABDT/source_video:/bus/darknet/source_video -e DISPLAY=$DISPLAY -e "Camera_id=4" ilnazsr/darknet


# Запускаем образ без экрана:
# docker run -it --gpus all --ipc=host -v /tmp/bus_log:/bus/darknet/result_video -e "Camera_id=1" shafigullinik/darknet

